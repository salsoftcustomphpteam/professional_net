<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['middleware' => ['json.response', 'localization']], function () {
	//Auth
	Route::post('auth/login', [App\Http\Controllers\Api\AuthController::class, 'login'])->name('login');
	Route::post('auth/register', [App\Http\Controllers\Api\AuthController::class, 'register'])->name('register');
	Route::post('auth/send/email', [App\Http\Controllers\Api\AuthController::class, 'sendForgotCode'])->name('sendForgotCode');
	Route::post('auth/verify/code', [App\Http\Controllers\Api\AuthController::class, 'verifiedForgotCode'])->name('verifiedForgotCode');
	Route::post('auth/new-password', [App\Http\Controllers\Api\AuthController::class, 'forgotPasswordChange'])->name('forgotPasswordChange');

	Route::get('admin/category', [App\Http\Controllers\Api\CategoryController::class, 'index'])->name('category.index');
	Route::post('admin/contactUs', [App\Http\Controllers\Api\AuthController::class, 'contactUs'])->name('user.contactUs');

	Route::get('user/downloadReume/{id}',[App\Http\Controllers\Api\JobController::class, 'downloadReume'])->name('downloadReume');
	Route::get('user/package', [App\Http\Controllers\Api\PackageManagmentController::class, 'getPackage'])->name('package.withAuthentication');


	Route::middleware('auth:api')->group( function () {
		Route::group(['prefix' => 'admin'], function () {

			//Auth
			Route::post('/logout', [App\Http\Controllers\Api\AuthController::class, 'logout']);
			Route::get('/getUser', [App\Http\Controllers\Api\AuthController::class, 'index'])->name('getUser');
			Route::post('/profile', [App\Http\Controllers\Api\AuthController::class, 'AdminProfile'])->name('AdminProfile');
			Route::post('/changePassword', [App\Http\Controllers\Api\AuthController::class, 'changePassword'])->name('changePassword');
			Route::get('/getProfile', [App\Http\Controllers\Api\AuthController::class, 'getProfile'])->name('getProfile');
			Route::get('/getProfile/{id}', [App\Http\Controllers\Api\AuthController::class, 'getProfile'])->name('getProfile.edit');
			Route::post('/user/create', [App\Http\Controllers\Api\AuthController::class, 'register'])->name('user.register');
			Route::post('/block-user', [App\Http\Controllers\Api\AuthController::class, 'blockedUser'])->name('user.block');
			Route::get('/block', [App\Http\Controllers\Api\AuthController::class, 'getBlockedUser'])->name('get.blocked.users');
			Route::post('/unblock-user', [App\Http\Controllers\Api\AuthController::class, 'unBlockedUser'])->name('get.unblocked.users');
			Route::post('/edit/user/{id}', [App\Http\Controllers\Api\AuthController::class, 'editUser'])->name('get.user.edit');
			Route::post('/update-user', [App\Http\Controllers\Api\AuthController::class, 'update_user'])->name('get.update_User');

			//packageManagment
			Route::get('/package', [App\Http\Controllers\Api\PackageManagmentController::class, 'getPackage'])->name('package');
			Route::get('/package/{id}', [App\Http\Controllers\Api\PackageManagmentController::class, 'getPackage'])->name('package.id');
			Route::post('/package/update', [App\Http\Controllers\Api\PackageManagmentController::class, 'update'])->name('package.update');

			//SubscriptionController
			Route::get('/subscriptions', [App\Http\Controllers\Api\SubscriptionController::class, 'index'])->name('subscriptions.index');
			Route::get('/subscriptions/{id}', [App\Http\Controllers\Api\SubscriptionController::class, 'index'])->name('subscriptions.edit');
			Route::get('/subscriptions/users/package', [App\Http\Controllers\Api\SubscriptionController::class, 'getCurrentUserPackage'])->name('subscriptions.getCurrentUserPackage');
			Route::post('/subscriptions/updateUserPackage', [App\Http\Controllers\Api\SubscriptionController::class, 'updateUserPackage'])->name('subscriptions.updateUserPackage');


			//PaymentLogController
			Route::get('/paymentlog', [App\Http\Controllers\Api\PaymentLogController::class, 'index'])->name('paymentlog.index');
			Route::get('/paymentlog/{id}', [App\Http\Controllers\Api\PaymentLogController::class, 'index'])->name('paymentlog.id');

			//JobLogController
			Route::post('jobs/create',[App\Http\Controllers\Api\JobController::class, 'create'])->name('job.create');
			Route::get('/joblog', [App\Http\Controllers\Api\JobController::class, 'index'])->name('joblog.index');
			Route::get('/joblog/{id}', [App\Http\Controllers\Api\JobController::class, 'index'])->name('joblog.id');
			Route::get('/candidate/job/{id}', [App\Http\Controllers\Api\JobController::class, 'jobCandidate'])->name('jobCandidate');
			Route::post('job/{id}',[App\Http\Controllers\Api\JobController::class, 'updateStatus'])->name('job.updateStatus');
			Route::post('job/candidate/status',[App\Http\Controllers\Api\JobController::class, 'jobStatus'])->name('job.status.create');
			Route::get('job/{job}/candidate/user',[App\Http\Controllers\Api\JobController::class, 'getJobCandidate'])->name('job.candidate');
			//Route::get('job/{job}/candidate/user',[App\Http\Controllers\Api\JobController::class, 'applyJob'])->name('job.applyJob');


			//Employee
			Route::post('/block-employee', [App\Http\Controllers\Api\EmployeeController::class, 'blockedEmployee'])->name('employee.block');
			Route::get('/blockedEmployee', [App\Http\Controllers\Api\EmployeeController::class, 'getBlockedEmployee'])->name('get.blocked.employee');
			Route::post('/unblock-employee', [App\Http\Controllers\Api\EmployeeController::class, 'unBlockedEmployee'])->name('get.unblocked.employee');
			Route::get('/employees', [App\Http\Controllers\Api\EmployeeController::class, 'index'])->name('employees.index');
			Route::get('/edit/employee/{id}', [App\Http\Controllers\Api\EmployeeController::class, 'editEmployee'])->name('employees.edit');
			Route::post('/employee-update', [App\Http\Controllers\Api\EmployeeController::class, 'employee_update'])->name('employees.update');
			Route::get('/getEmployee/{id}', [App\Http\Controllers\Api\EmployeeController::class, 'getEmployee'])->name('employees.getEmployee');
			Route::get('/statics', [App\Http\Controllers\Api\EmployeeController::class, 'statics'])->name('employees.statics');

			//questionaires
			Route::get('/getQuestionaire', [App\Http\Controllers\Api\QuestionaireController::class, 'index'])->name('questionaires.index');
			Route::post('questionaire/create', [App\Http\Controllers\Api\QuestionaireController::class, 'create'])->name('questionaires.create');

			//homecontroller
			Route::get('/dashboardStatic', [App\Http\Controllers\Api\AuthController::class, 'dashboardStatic'])->name('dashboardStatic');
			Route::get('/adminDashboardStatic', [App\Http\Controllers\Api\AuthController::class, 'adminDashboardStatic'])->name('adminDashboardStatic');


			//instituteManagment
			Route::get('/institute', [App\Http\Controllers\Api\InstitutesManagment::class, 'index'])->name('institute.index');

			//category
			Route::post('category/create', [App\Http\Controllers\Api\CategoryController::class, 'create'])->name('category.create');
			Route::post('category/update', [App\Http\Controllers\Api\CategoryController::class, 'update'])->name('category.update');
			
			//Route::get('category', [App\Http\Controllers\Api\CategoryController::class, 'index'])->name('category.index');
			//Network and Training
			Route::post('networktraining/create', [App\Http\Controllers\Api\NetworkTrainingController::class, 'create'])->name('networktraining.create');
			Route::post('networktraining/update', [App\Http\Controllers\Api\NetworkTrainingController::class, 'update'])->name('networktraining.update');
			Route::get('networktraining', [App\Http\Controllers\Api\NetworkTrainingController::class, 'index'])->name('networktraining.index');

			//notification

			Route::get('getNotification', [App\Http\Controllers\Api\AuthController::class, 'getNotification'])->name('auth.getNotification');
		});

		Route::group(['prefix' => 'user'], function () {
		    Route::post('cvBuilder',[App\Http\Controllers\Api\JobController::class, 'cvBuilder'])->name('job.cvBuilder');
		    Route::get('cv',[App\Http\Controllers\Api\JobController::class, 'getUserCv'])->name('job.getUserCv');
		    Route::post('cv/update',[App\Http\Controllers\Api\JobController::class, 'cvBuilderUpdate'])->name('job.cvBuilderUpdate');
		    Route::get('cv/delete',[App\Http\Controllers\Api\JobController::class, 'deleteCv'])->name('job.deleteCv');
		    Route::get('jobs/{status}',[App\Http\Controllers\Api\JobController::class, 'userAppliedJob'])->name('job.userAppliedJob');
		    Route::get('job/{job}/user',[App\Http\Controllers\Api\JobController::class, 'applyJob'])->name('job.applyJob');
		    //Route::get('jobs/{status}',[App\Http\Controllers\Api\JobController::class, 'jobList'])->name('job.userAppliedJob');
		    Route::get('network/training/getById/{id}',[App\Http\Controllers\Api\NetworkTrainingController::class, 'getById'])->name('network.training.getById');
		    Route::get('network/training/registerNetworkTraining/{id}',[App\Http\Controllers\Api\NetworkTrainingController::class, 'registerNetworkTraining'])->name('network.training.registerNetworkTraining');
		    
        });


       
	});
});
