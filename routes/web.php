<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome')->where('any', '.*');;
// });

Route::redirect('/', 'user/welcome', 301);

// Auth::routes();
Route::get('admins/{path}',[App\Http\Controllers\HomeController::class, 'index'])->name('index')->where('path','.*');
//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->where('any', '.*');
Route::get('business/{path}',[App\Http\Controllers\HomeController::class, 'business'])->name('business')->where('path','.*');
Route::get('user/{path}',[App\Http\Controllers\HomeController::class, 'user'])->name('user')->where('path','.*');

