<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Professional Net</title>
    <base href="{{url('/')}}">
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('assets/app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('assets/app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('assets/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('assets/app-assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <!-- BEGIN Custom CSS-->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/assets/css/style.css')}}">
{{--    <link rel="stylesheet" type="text/css" href="{{url('assets/assets/css/login.css')}}">--}}

    <link rel="stylesheet" type="text/css" href="{{url('assets/assets/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/assets/css/CustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/assets/css/circle.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/croppie/croppie.css">
{{--    <script>--}}
{{--               window.Laravel = {!!json_encode([--}}
{{--                   'isLoggedin' => true,--}}
{{--                   'user' => null,--}}
{{--                   'as' => 'public',--}}
{{--               ])!!}--}}
{{--            </script>--}}
    <script>
        window.base_url = '{{url('/')}}';
    </script>
</head>
<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    <div id="app">
    </div>
</body>
<script src="{{ url(mix('/js/app.js')) }}"></script>

<!-- BEGIN VENDOR JS-->
<script src="{{url('assets/app-assets/vendors/js/vendors.min.js')}}" ></script>
<script src="{{url('assets/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/vendors/js/charts/echarts/chart/bar.js')}}" type="text/javascript"></script>

<script src="{{url('assets/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/js/scripts/charts/echarts/bar-column/basic-column.js')}}" type="text/javascript"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/vue-croppie@2.0.2/dist/vue-croppie.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{url('assets/app-assets/vendors/js/extensions/moment.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/vendors/js/extensions/fullcalendar.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/js/scripts/extensions/fullcalendar.js')}}" type="text/javascript"></script>

<script src="{{url('assets/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
<script src="{{url('assets/app-assets/js/scripts/modal/components-modal.j')}}s" type="text/javascript"></script>
<script src="{{url('assets/assets/js/function.js')}}"></script>

</html>
