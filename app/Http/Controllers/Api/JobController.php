<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Job\CreateJobRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use Barryvdh\Snappy\Facades\SnappyImage;
// use Barryvdh\Snappy\SnappyPDF;
//pdf
use PDF;
//models
use App\Models\JobLog;
use App\Models\JobAppliedCandidate;

//Libraries
use App\Http\Controllers\Api\BaseController;
use App\Models\Cv;
use App\Models\UserEducation;
use App\Models\UserExperience;

class JobController extends BaseController
{
    //
    public function index(Request  $request, $id = null)
    {
        //dd($request->hours_of_work);
        $date = !!$request->to &&  $request->to != "null" && $request->from && $request->from != "null";
        $searchTerm = $request->title;
        $city = $request->city;

        $hours = !!$request->hours_of_work && $request->hours_of_work != "null";
        $years = !!$request->year_of_expereince && $request->year_of_expereince != "null";
        $education_training = !!$request->education_training && $request->education_training == "null";
        $category = !!$request->category && $request->category != "null";
        $period_of_employment = !!$request->period_of_employment && $request->period_of_employment != "null";
        $salary = !!$request->salary && $request->salary != "null";


        isset($request->hit_from) ? $id = auth()->user()->id: '';

        if($id != null)
        {

            return JobLog::where('user_id',$id)->when(!!$date ,function($q) use($request){
                $q->whereBetween('apply_before',[Carbon::parse($request->from)->format('Y-m-d'),Carbon::parse($request->to)->format('Y-m-d')]);
            })->when($request->filled('search'),function($q)use($request){

                $q->where('job_title', 'like', '%' . $request->search . '%');
            })->paginate($request->entries);
        }

        return JobLog::when(!!$date ,function($q) use($request){
            $q->whereBetween('apply_before',[Carbon::parse($request->from)->format('Y-m-d'),Carbon::parse($request->to)->format('Y-m-d')]);
        })->when(!!$hours,function($q) use($request){
            $q->where('hours_of_work',$request->hours_of_work);
        })->when(!!$salary ,function($q) use($request){
            $q->where('salary_range','>',$request->salary);
        })->when(!!$years,function($q) use($request){
            $q->where('years_of_experience',$request->year_of_expereince);
        })->when(!!$education_training,function($q) use($request){
            $q->where('degree',$request->education_training);
        })->when(!!$category,function($q) use($request){
            $q->where('category_id',$request->category);
        })->when(!!$period_of_employment,function($q) use($request){
            $q->where('period_of_employment',$request->period_of_employment);
        })->when(!!$searchTerm,function($q) use($request){
            $q->where('job_title', 'LIKE', "%{$request->title}%");
        })->when(!!$city,function($q) use($request){
            $q->where('city', 'LIKE', "%{$request->city}%");
        })->where('status',1)->paginate($request->entries);
    }

    public function jobCandidate(Request $request,$id)
    {
        return JobLog::with(['jobAppliedCandidate','jobAppliedCandidate.user'])->where('id',$id)->paginate($request->entries);
            //JobAppliedCandidate::with(['job','user'])->where('job_id',$id)->paginate($request->entries);
    }

    public function create(CreateJobRequest $request)
    {
        $jobData = $request->all();
        //dd($jobData);
        $file = $this->uploadFile();
        unset($jobData['file']);
        $jobData['file'] = $file;
        $jobData['user_id'] = auth()->user()->id;
        $job_log = JobLog::create($jobData);

        if($job_log){
          return $this->sendResponse($job_log, __('responseMessages.createJobSuccessfully'));
      } else{
        return $this->sendError('Error creating job');
    }

}
public function getJobCandidate($job)
{
    return JobAppliedCandidate::with(['job','user'])->where('job_id',$job)->where('user_id',auth()->id())->first();
}

public function jobStatus(Request $request)
{
    JobAppliedCandidate::where('job_id',$request->job_id)->where('user_id',$request->user_id)->update([
        'status' => $request->status
    ]);

    return $this->sendResponse(true,__('responseMessages.jobStatusUpdatedSuccessfully'));

}
public function updateStatus(Request $request,$id)
{
    JobLog::whereId($id)->update([
        'status' => $request->job_status
    ]);

    return $this->sendResponse(true,__('responseMessages.jobStatusUpdatedSuccessfully'));
}

public function userAppliedJob(Request $request,$status)
{
    $date = !!$request->to &&  $request->to != "null" && $request->from && $request->from != "null";
    $statusCheck  = ($status === "all") ? false : true; 

    $jobLogs = JobAppliedCandidate::with('job')->where('user_id',auth()->user()->id)->when(!!$statusCheck,function($q)use($status){
        $q->where('status',$status);
    })->when(!!$date ,function($q) use($request){
        $q->whereBetween('created_at',[Carbon::parse($request->from)->format('Y-m-d'),Carbon::parse($request->to)->format('Y-m-d')]);
    })->get();

    return $this->sendResponse($jobLogs,__('responseMessages.userJobs'));
}

public function applyJob($job)
{
    JobAppliedCandidate::create([
        'job_id' => $job,
        'user_id' => auth()->user()->id
    ]);

    $this->createNotification(auth()->id(),'Models/App/User',"You applied a job","You applied a job",null,'user');

    return $this->sendResponse(true,__('responseMessages.jobAppliedSuccessfully'));
}

public function cvBuilder(Request $request)
{
    $cvs = Cv::create([
        'first_name' => $request->first_name,
        'last_name'  => $request->last_name,
        'dob'        => $request->dob,
        'phone_number' => $request->phone_number,
        'email'      => $request->email,
        'profession' => $request->profession,
        'interest_subject' => $request->interest_selected,
        'interest_desc' => $request->interest,
        'lang'      => $request->language,
        'user_id'  => auth()->id()
    ]);

    $education = json_decode($request->educations);
    $experience = json_decode($request->experiences);
        //dd($education);
    for ($i=0; $i < count($education); $i++) { 
        UserEducation::create([
            'degree' => $education[$i]->degree,
            'subject' =>$education[$i]->subjects,
            'institute_name' => $education[$i]->institute_name,
            'start_date'     => $education[$i]->start_date,
            'end_date'      => $education[$i]->end_date,
            'cvs_id'      => $cvs->id,
            'user_id'       => auth()->id()
        ]);
    }
    for ($i=0; $i < count($experience); $i++) { 
        UserExperience::create([
            'job_title' => $experience[$i]->job_title,
            'company_name' => $experience[$i]->company_name,
            'job_type'  => $experience[$i]->job_type,
            'start_date' => $experience[$i]->start_date,
            'end_date'   => $experience[$i]->end_date,
            'description' => $experience[$i]->description,
            'cvs_id'      => $cvs->id,
            'user_id'       => auth()->id()
        ]);
    }
    // $pdf = PDF::loadView('resume.index');
    // return $pdf->download('pdfview.pdf');
    return $this->sendResponse(true,__('responseMessages.cvCreatedSuccessfully'));

}
public function cvBuilderUpdate(Request $request)
{
       // dd($request);

    $cvs1 = Cv::where('user_id',auth()->id())->update([
        'first_name' => $request->first_name,
        'last_name'  => $request->last_name,
        'dob'        => $request->dob,
        'phone_number' => $request->phone_number,
        'email'      => $request->email,
        'profession' => $request->profession,
        'interest_subject' => $request->interest_subject,
        'interest_desc' => $request->interest_desc,
        'lang'      => $request->lang
    ]);
    $cvs = Cv::where('user_id',auth()->id())->first();
    UserEducation::where('user_id',auth()->id())->delete();
    UserExperience::where('user_id',auth()->id())->delete();
    $education = $request->education;
    $experience = $request->experience;
        //dd($education[0]['degree']);
    for ($i=0; $i < count($education); $i++) {

        UserEducation::create([
            'degree' => $education[$i]['degree'],
            'subject' => $education[$i]['subject'],
            'institute_name' => $education[$i]['institute_name'],
            'start_date'     => $education[$i]['start_date'],
            'end_date'      => $education[$i]['end_date'],
            'cvs_id'      => $cvs->id,
            'user_id'       => auth()->id()
        ]);
    }
    for ($i=0; $i < count($experience); $i++) { 
        UserExperience::create([
            'job_title' => $experience[$i]['job_title'],
            'company_name' => $experience[$i]['company_name'],
            'job_type'  => $experience[$i]['job_type'],
            'start_date' => $experience[$i]['start_date'],
            'end_date'   => $experience[$i]['end_date'],
            'description' => $experience[$i]['description'],
            'cvs_id'      => $cvs->id,
            'user_id'       => auth()->id()
        ]);
    }

   // $pdf = PDF::loadView('resume.index');
    // return $pdf->download('pdfview.pdf');

    return $this->sendResponse(true,__('responseMessages.cvCreatedSuccessfully'));

}

public function getUserCv()
{
    $cvs = Cv::where('user_id',auth()->id())->first();
    return $this->sendResponse($cvs,__('responseMessages.cvFetchedSuccessfully'));
}

public function deleteCv()
{
    Cv::where('user_id',auth()->id())->delete();
    UserEducation::where('user_id',auth()->id())->delete();
    UserExperience::where('user_id',auth()->id())->delete();
    return $this->sendResponse(true,__('responseMessages.deleteCvSuccessfully'));
}

public function downloadReume($id)
{
    $data = Cv::where('user_id',$id)->first();
    //dd($data);
    $pdf = PDF::loadView('resume.index',['data'=>$data]);
    return $pdf->download('resume.pdf');

}

private function generatePDF($props = []) {
        // $author   = array_key_exists('author', $props) ? $props['author'] : $this->author;
    // $data     = array_key_exists('data', $props) ? $props['data'] : json_decode($this->data);
    // $template = array_key_exists('template', $props) ? $props['template'] : $this->template;
    // $title    = array_key_exists('title', $props) ? $props['title'] : $this->title;

        // Extract out the contact information from the data so it can be
        // reused easily whenever required in the future by the templates.
        // $contact_info = self::extractContactInfo($data);

    return \PDF::loadView('resume.index',$props)
    ->setPaper('a4');

    
}

}
