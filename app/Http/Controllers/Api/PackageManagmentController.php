<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//Models
use App\Models\PackageManagment;

// Requests
use App\Http\Requests\PackageManagment\CreatePackageRequest;

// Interfaces
use App\Repositories\PackageManagment\PackageManagmentInterface;

//Libraries
use App\Http\Controllers\Api\BaseController;

class PackageManagmentController extends BaseController
{
	/**
     * PackageManagmentController constructor.
     *
     * @param  PackageManagmentInterface  $packagemanagment
     */
	public function __construct(PackageManagmentInterface $package)
    {
        $this->package = $package;
    }
     /**
     * PackageManagment create.
     *
     * @param  CreatePackageRequest  $request
     */

    public function update(CreatePackageRequest $request)
    {
        $package = $this->package->update($request->only(
            'package_name',
            'charges',
            'package_id',
            'package_description'
        ));

        if (isset($package)) {
            return $this->sendResponse(true, __('messages.success_register'));
        }

        return $this->sendError('Unable to register user.');

    }

     /**
     * PackageManagment getPackage.
     *
     * @param  Request  $request
     */

    public function getPackage(Request $request,$id=null)
    {
        $userType = (isset($request->user_type)) ? $request->user_type : $id;

    	$package = PackageManagment::where('user_type',$userType)->get();


        if (isset($package)) {
            return $this->sendResponse($package, __('responseMessages.fetchPackageSuccessfully'));
        }

        return $this->sendError(__('responseMessages.errorFetchingPackage'));

    }
}
