/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.3.16-MariaDB : Database - intellect_bay
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`intellect_bay` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `intellect_bay`;

/*Table structure for table `employees` */

CREATE TABLE `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'avatar.jpg',
  `phone_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institute_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `block_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `employees` */

insert  into `employees`(`id`,`first_name`,`last_name`,`email`,`image`,`phone_code`,`phone`,`address`,`country`,`state`,`city`,`zip_code`,`institute_id`,`package_id`,`block_status`,`created_at`,`updated_at`) values (1,'Touqeer','Hussain','hussain@gmail.com','avatar.jpg',NULL,'+923402111701','Malir','Pakistan','Sindh','karachi','75210',1,1,1,'2020-11-19 17:32:22','2020-11-19 14:51:38');

/*Table structure for table `failed_jobs` */

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `migrations` */

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2016_01_15_105324_create_roles_table',2),(10,'2016_01_15_114412_create_role_user_table',2),(11,'2016_01_26_115212_create_permissions_table',2),(12,'2016_01_26_115523_create_permission_role_table',2),(13,'2016_02_09_132439_create_permission_user_table',2),(14,'2020_11_14_104942_alter_users_add_soft_detele',3),(15,'2020_11_16_080915_alter_add_forgot_code_users',4),(16,'2020_11_16_145542_alter_add_address_details_users',4),(17,'2020_11_17_123720_create_package_managments_table',4),(18,'2020_11_18_054555_alter_users_table',4),(19,'2020_11_18_103922_alter_add_block_status_users_table',4),(20,'2020_11_19_110618_create_employees_table',5),(21,'2020_11_19_112316_create_subscription_logs_table',5);

/*Table structure for table `oauth_access_tokens` */

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_access_tokens` */

insert  into `oauth_access_tokens`(`id`,`user_id`,`client_id`,`name`,`scopes`,`revoked`,`created_at`,`updated_at`,`expires_at`) values ('036962c05c2905252d5c011e4549d664e9df287d5d8f3393aeb2016d5680905ac494ee586b52b742',1,1,'Laravel','[]',0,'2020-11-15 09:11:11','2020-11-15 09:11:11','2021-11-15 09:11:11'),('08297d91ee9896d5939ee5b52e8653f640d57b0bbf2514d785cce28f8cbee6669623887bd16ccac0',1,1,'Laravel','[]',0,'2020-11-15 11:30:01','2020-11-15 11:30:01','2021-11-15 11:30:01'),('0aebfa0712af1a32ac5464ced1c0876e76f73598bc3350e254c0d2bb8876ef5cad9190b4779140ee',1,1,'Laravel','[]',0,'2020-11-15 11:35:58','2020-11-15 11:35:58','2021-11-15 11:35:58'),('0ca79d722fb559e5997781949d6f7d1440e92c431b83931c67f03e27c3fd2692bdd4250074fb3121',1,1,'Laravel','[]',0,'2020-11-15 07:24:50','2020-11-15 07:24:50','2021-11-15 07:24:50'),('0df00eedf12961d005ee31b9ed51a6690bfa28d2d94aedfab857d7a700f70746345ea3b106e0ac15',1,1,'Laravel','[]',0,'2020-11-15 09:54:46','2020-11-15 09:54:46','2021-11-15 09:54:46'),('0f0b110a9473b4217fa8a00c8ba22d156843adad230cdf17a7144e33a21adbb970366a0fd0273730',1,1,'Laravel','[]',0,'2020-11-15 08:53:19','2020-11-15 08:53:19','2021-11-15 08:53:19'),('13b31c931da4ea868f35c57ab3af3f896f22a686088dee86ea4561ad57032e93e9c1c54876cf21b8',1,1,'Laravel','[]',0,'2020-11-15 07:46:11','2020-11-15 07:46:11','2021-11-15 07:46:11'),('160b44b1eb324032213dbc4d4c446df1d2995ebd06e0a16de0631d846e2c9d867a57cb2a934a80e7',1,1,'Laravel','[]',1,'2020-11-16 18:15:05','2020-11-16 18:15:05','2021-11-16 18:15:05'),('178e5dcdf7d233a53dd4b29e1d5aaee608ad52a87fea13ab5265025738a5dacbcae2c73f78a59fc6',1,1,'Laravel','[]',0,'2020-11-15 18:52:37','2020-11-15 18:52:37','2021-11-15 18:52:37'),('1dd1165010f6cc4b686f2036907977b7772921b07d6b9385990f73a894e5306c5801635a51da2731',1,1,'Laravel','[]',0,'2020-11-15 04:17:55','2020-11-15 04:17:55','2021-11-15 04:17:55'),('201a3084f7d05167ec7c7aa98a41835d66bdbf3f047450f7456d4fad993f3d6ee7d48b1d4d187b30',1,1,'Laravel','[]',0,'2020-11-15 10:55:29','2020-11-15 10:55:29','2021-11-15 10:55:29'),('21efeab3845caeba4a44228a2c6e2b53a20c4334c8e944bb4d996f72a20a3d0a64071666960e75df',1,1,'Laravel','[]',0,'2020-11-15 10:39:55','2020-11-15 10:39:55','2021-11-15 10:39:55'),('294c1419fc28895bdfffe65baa6f76890c93a7b40ef0394c90c36142912e5c2c6a2c34950d483af5',1,1,'Laravel','[]',0,'2020-11-15 07:22:22','2020-11-15 07:22:22','2021-11-15 07:22:22'),('2a2497c54a150fb92137ae8f88ec17d6f2a8e974ba5a9874bdc10693f93bd6e874bfdb1cdbe821f5',1,1,'Laravel','[]',0,'2020-11-16 18:36:06','2020-11-16 18:36:06','2021-11-16 18:36:06'),('2feac4379c0ec172fb39a1e5d432e1fd1351427493baf28a15be686f4115d23561e4b50aa85905a9',1,1,'Laravel','[]',0,'2020-11-14 11:01:41','2020-11-14 11:01:41','2021-11-14 11:01:41'),('31799ce5b69ee51e5e9a773ee6f32145b275d3ad962ec57b1fd461881de06d02f6beefa246355b5f',1,1,'Laravel','[]',0,'2020-11-15 08:54:33','2020-11-15 08:54:33','2021-11-15 08:54:33'),('319fb7757fd0b805835ce0641ee654c5f4667c5e4040f2b78b376f8af7bda4072714e7c29f6d40ff',1,1,'Laravel','[]',0,'2020-11-15 09:26:22','2020-11-15 09:26:22','2021-11-15 09:26:22'),('345ff3593f77bb7503774b6fb204821298091bfd2165ceb32e449aeac23b40e9ffb9896e429f03c9',1,1,'Laravel','[]',0,'2020-11-15 09:22:54','2020-11-15 09:22:54','2021-11-15 09:22:54'),('34d999d3423cf8bd2bd115d4055757334e732735137aa8d8b21c0f74d94f1d45c21630333b80d715',1,1,'Laravel','[]',0,'2020-11-15 08:51:56','2020-11-15 08:51:56','2021-11-15 08:51:56'),('36b5c20c6dcbb8f135d6eee482c1c7dc754863274dfb5334b5340d140afd6912b727bada5109d38d',1,1,'Laravel','[]',0,'2020-11-15 09:28:17','2020-11-15 09:28:17','2021-11-15 09:28:17'),('3a38707f14e4ac740f37db1078a37cdea759bf23bc8eac88cc7baf32a9cda040cd2dfe52732dbdef',1,1,'Laravel','[]',0,'2020-11-15 07:56:25','2020-11-15 07:56:25','2021-11-15 07:56:25'),('3bcb0310ff0a4c2cb07a9f310c7251874f31fea1133c2368e67ea38679ea0ad9dd30f2bea36f7a39',1,1,'Laravel','[]',0,'2020-11-15 09:05:28','2020-11-15 09:05:28','2021-11-15 09:05:28'),('3bfd569f54abecd8c2dd746084f656322b85e074fb67ad2231271829bc5f075fe6fc7262d20083e8',1,1,'Laravel','[]',1,'2020-11-16 18:21:30','2020-11-16 18:21:30','2021-11-16 18:21:30'),('3cf0f4220284f8d62c89e01910f94c151f89f701a28d627c277221d42bb70617ae4054bafcb0ad24',1,1,'Laravel','[]',0,'2020-11-16 19:07:28','2020-11-16 19:07:28','2021-11-16 19:07:28'),('4174a77efc49f66f884b0ffc30bfbac2937e3bb612752f58aa390cb39c790cf7993c97f933b070d0',1,1,'Laravel','[]',1,'2020-11-15 17:54:45','2020-11-15 17:54:45','2021-11-15 17:54:45'),('41b1d522125b1ab9abe6a64edc2f75a3143b71d85b04ecd8d0a421c358d1b477d460787a3452fd46',1,1,'Laravel','[]',0,'2020-11-15 09:12:40','2020-11-15 09:12:40','2021-11-15 09:12:40'),('43ab71f318a93c1073aa3b6b3c23f41f04192d41e184c306e70263a224fd14978f5fa0a37fbf1581',1,1,'Laravel','[]',0,'2020-11-15 10:38:52','2020-11-15 10:38:52','2021-11-15 10:38:52'),('44a2ab266f3ae4e7bc2fbb7ed10f79fd11cf382d1a672236a982790b48f6f432d25663ecdf064c45',1,1,'Laravel','[]',0,'2020-11-14 11:22:10','2020-11-14 11:22:10','2021-11-14 11:22:10'),('467b56bb313854413415189e5fbc8265ca3fac04591fbafdb773feadd1a102a24762679d78013521',1,1,'Laravel','[]',0,'2020-11-15 08:00:28','2020-11-15 08:00:28','2021-11-15 08:00:28'),('49996199e4010c93d069b92becd2f54b6951174b5f3c987b318734f3b2b54dc188c629fb58198300',1,1,'Laravel','[]',0,'2020-11-15 09:57:16','2020-11-15 09:57:16','2021-11-15 09:57:16'),('4edddc9ead2fd9951e97ac5a55c0918a21147df3089340cd28506956a6834f26887a3d95ba92af2b',1,1,'Laravel','[]',0,'2020-11-15 10:53:10','2020-11-15 10:53:10','2021-11-15 10:53:10'),('506e2723a0a003317dee793da464321a8b0e681c87bfe70ff506e9a844fd3ca30571657eeb7d1113',1,1,'Laravel','[]',0,'2020-11-15 18:52:39','2020-11-15 18:52:39','2021-11-15 18:52:39'),('52dd0a176c4ba83c7be95f38a7dadab36225ca68d04d5f071a0120835741788b0167f9ad2ef99894',1,1,'Laravel','[]',0,'2020-11-15 04:20:01','2020-11-15 04:20:01','2021-11-15 04:20:01'),('540a4803732aa4bc7a192cfe55f19bcb7dbba41ad12e55140ef22aaa0e4cbf846303d70052e504f1',1,1,'Laravel','[]',0,'2020-11-16 18:11:34','2020-11-16 18:11:34','2021-11-16 18:11:34'),('553c16394816f25c3e8b90c0e5baecc8e24e490f1fe79e97c2062af9af6a20c657be7a5b99f7d70d',1,1,'Laravel','[]',0,'2020-11-15 17:31:25','2020-11-15 17:31:25','2021-11-15 17:31:25'),('575c5ee08785ed28e987a2ada06f624019d33f4fcaf654b7d25782350806c98853f9a9dd1cca8663',1,1,'Laravel','[]',0,'2020-11-15 04:21:26','2020-11-15 04:21:26','2021-11-15 04:21:26'),('57f175de5833af6915d3bca1d4759d797da53737679ad10cec7a2593fb16b841d41551a40a8cc1e2',1,1,'Laravel','[]',0,'2020-11-15 10:29:03','2020-11-15 10:29:03','2021-11-15 10:29:03'),('580291054e8ee3f1ec2fc391434e34b85957fb771086626670d34d1ac03be7d42c3484106686de2e',1,1,'Laravel','[]',0,'2020-11-15 08:42:29','2020-11-15 08:42:29','2021-11-15 08:42:29'),('589a378e758f14fe7a81512aee00b2fd14b6f311936b9ce29ddc8188d9cfeb39a81e7ef93a151ff1',1,1,'Laravel','[]',0,'2020-11-15 12:15:17','2020-11-15 12:15:17','2021-11-15 12:15:17'),('5a9722d58e53c80d1a87fe988b639fa61acdc594721486f862277c6053426c0875ed6ac4959f4770',1,1,'Laravel','[]',0,'2020-11-15 10:30:09','2020-11-15 10:30:09','2021-11-15 10:30:09'),('5c25c04dd8f27686c2ee2cfcae190ece86dd74eb318291b68ec6554bfba72b228f891b0453474cf3',1,1,'Laravel','[]',0,'2020-11-15 04:24:07','2020-11-15 04:24:07','2021-11-15 04:24:07'),('5d630e25177ccccdadc7423bb07bf31383b14680a6cf2079184766f5da2b7ebb8437e6ee89e2e992',1,1,'Laravel','[]',0,'2020-11-15 07:35:02','2020-11-15 07:35:02','2021-11-15 07:35:02'),('5ed6747f15b586fa7bf26656ca2d19923cd01b18a7d0482a2a1235eebdb9792c0cd0f0391f4781cd',1,1,'Laravel','[]',0,'2020-11-15 09:54:08','2020-11-15 09:54:08','2021-11-15 09:54:08'),('5fba63a32a4b71968a372b4010d644cea7dcbe69be9510b823b1fb0e1e442dccaa2182d1291397a3',1,1,'Laravel','[]',0,'2020-11-15 07:49:59','2020-11-15 07:49:59','2021-11-15 07:49:59'),('6033be17ffd4dad86cd2e6f0e471c4ef5a7e0a2bdf719fa1fed615df1005475726218394a3f46110',1,1,'Laravel','[]',0,'2020-11-15 09:53:40','2020-11-15 09:53:40','2021-11-15 09:53:40'),('62a4c9ba36f58646e60072549b813ba9a5fe50685ecacbc5005b07315c0a4128bd509a2b9e8ac38a',1,1,'Laravel','[]',0,'2020-11-15 09:29:56','2020-11-15 09:29:56','2021-11-15 09:29:56'),('62b08f421be1b4d86996a737fdc6f33ba41f2a8b5b7d38fc988bcac6408b2770d90acfd7e4dd53e3',1,1,'Laravel','[]',0,'2020-11-15 09:19:23','2020-11-15 09:19:23','2021-11-15 09:19:23'),('6760ab16351c6950ba6601ec35c0a8819adf748edf2b9b73dede6015d25e34c655c9f860949316c3',1,1,'Laravel','[]',0,'2020-11-15 08:49:28','2020-11-15 08:49:28','2021-11-15 08:49:28'),('68aefa0cb8603a11ffe35b3e0bcd0318c7a40c086a67bed4e3288efcba625b9c22e7ac7dc25f9252',1,1,'Laravel','[]',0,'2020-11-15 09:15:25','2020-11-15 09:15:25','2021-11-15 09:15:25'),('68ced62bfcd1960eba2fa692a54b1c84b9bdd280bfc288943367d0f2a321b11394206bf7d6c9c3ca',1,1,'Laravel','[]',0,'2020-11-15 10:33:41','2020-11-15 10:33:41','2021-11-15 10:33:41'),('694b49e383b9ac864dac6072cdb1ac64a4b21b46417277aede6c330ff7e0c0b0791db36a17092c68',1,1,'Laravel','[]',0,'2020-11-15 07:20:38','2020-11-15 07:20:38','2021-11-15 07:20:38'),('6973bb93a943e575d2c19250e96de73efb7e8cf06222883910ce97958787f949a15ecfac7f8901ad',1,1,'Laravel','[]',0,'2020-11-15 04:23:39','2020-11-15 04:23:39','2021-11-15 04:23:39'),('697dcee6722ddc009198c6e06b215a3ade5e5533b4f3d2d071a3f999ec640b324813cd33b318cfca',1,1,'Laravel','[]',0,'2020-11-15 07:50:23','2020-11-15 07:50:23','2021-11-15 07:50:23'),('6acd38f9d3bb5ab2e1b98f54b6344c3e48ee7c44a12ee51262febe198640384de4cb2f74ec723a01',1,1,'Laravel','[]',0,'2020-11-15 10:32:30','2020-11-15 10:32:30','2021-11-15 10:32:30'),('6b82d1dfd7e9d9c46651426e34a82bf1f308be69e7e81c3cee5b81782ae52e275a69b08b44923300',1,1,'Laravel','[]',1,'2020-11-16 18:23:19','2020-11-16 18:23:19','2021-11-16 18:23:19'),('75c499d75a1792b18ad38ab5908ea2c4653267ff49172fbc5cd65c21a9c636cbc8af08670a94d593',1,1,'Laravel','[]',0,'2020-11-15 08:50:34','2020-11-15 08:50:34','2021-11-15 08:50:34'),('76aa41863ea43729242147aa600da6b33d8e2961ae8504050cae70ed8af4dff1d74ce6cf567e10c9',1,1,'Laravel','[]',1,'2020-11-18 20:50:29','2020-11-18 20:50:29','2021-11-18 20:50:29'),('76d64d591fc2a617323d5862a7c0b684e403adae00584820f662253b10634997bb3d64f630a39cea',1,1,'Laravel','[]',0,'2020-11-15 09:13:41','2020-11-15 09:13:41','2021-11-15 09:13:41'),('772eaf07d3d1a8fae4db7211bc2c9fc45e0aec916510004eed0c45096adfb3be80e94a27d75c9dd1',1,1,'Laravel','[]',0,'2020-11-15 09:51:50','2020-11-15 09:51:50','2021-11-15 09:51:50'),('797596a3189c75c6e531234f342ad0d645c8ba89c454e9d28b0241aa935f168c67c1eb40cdc11a21',1,1,'Laravel','[]',0,'2020-11-15 12:05:05','2020-11-15 12:05:05','2021-11-15 12:05:05'),('7dae9ced7062a93274ef75ba69ec34e60e1dc318f6f0c0178ed17c484c1e612389c3dfc001acaf44',1,1,'Laravel','[]',0,'2020-11-15 07:58:14','2020-11-15 07:58:14','2021-11-15 07:58:14'),('816b5c239aa693e87407b7842f034306bbb701bee7b086b02a5c9ee1b414066a87bc8718b7acf065',1,1,'Laravel','[]',0,'2020-11-15 09:50:25','2020-11-15 09:50:25','2021-11-15 09:50:25'),('82ea0e447a07c92eb85738c51b18705bb690ab1782e47949527b8da06db2713861d781adcd4a586b',1,1,'Laravel','[]',0,'2020-11-15 08:58:02','2020-11-15 08:58:02','2021-11-15 08:58:02'),('834db17d1f12b37d83a577dba51714bd042448d0f068f3950586052458b4f25f4807d149bbc4360d',1,1,'Laravel','[]',0,'2020-11-16 18:27:02','2020-11-16 18:27:02','2021-11-16 18:27:02'),('83c110901adad38325128d1e3d6d26d9424582603d7e4672973932d16553d060d29550c3ffe13477',1,1,'Laravel','[]',0,'2020-11-15 07:45:48','2020-11-15 07:45:48','2021-11-15 07:45:48'),('849c264ecaffe2178b2026dc00196585b21056b14a380bb3ad35cc83b0579b9a77d254186b28ce40',1,1,'Laravel','[]',0,'2020-11-15 09:49:18','2020-11-15 09:49:18','2021-11-15 09:49:18'),('87246c4f995cdb4b99c295079f34bd0543532e2afce8fcd5fd359db42974934fc425eabfb356935b',1,1,'Laravel','[]',1,'2020-11-16 19:06:56','2020-11-16 19:06:56','2021-11-16 19:06:56'),('872db023782710a8efb22186d2b3327be15d17ce0f9b28300cc3a030a3818fe462888b28e978124e',1,1,'Laravel','[]',0,'2020-11-15 09:45:41','2020-11-15 09:45:41','2021-11-15 09:45:41'),('8a9fcf58d02f198352dc3eeb6c90be6b1c44993572fe8878c6756cf5a61eb189f9390cbf7e9f3bcf',1,1,'Laravel','[]',1,'2020-11-18 21:13:30','2020-11-18 21:13:30','2021-11-18 21:13:30'),('8ab2c765362b78e873ae1a1952df4cfa86b646c7e77f8d59ff6cafd75930421c349e6280bc59b940',1,1,'Laravel','[]',0,'2020-11-15 04:21:56','2020-11-15 04:21:56','2021-11-15 04:21:56'),('93e3678e4e5036b65b62398835bceceaae86deb6cd6de8023e4554bdb895ae9a1d8f9dad65209305',1,1,'Laravel','[]',0,'2020-11-15 11:51:49','2020-11-15 11:51:49','2021-11-15 11:51:49'),('94846816d49a4f02e844f91e22c40045f8a09f035022c7f80e482cf3ff6f3ec92942537b2ba957ef',1,1,'Laravel','[]',1,'2020-11-15 17:32:05','2020-11-15 17:32:05','2021-11-15 17:32:05'),('99325660903272a86d3c2a9083d23f01260af55e50e43d91cf82d7a27f8eb3d6d260b8509e47fa75',1,1,'Laravel','[]',0,'2020-11-16 18:25:26','2020-11-16 18:25:26','2021-11-16 18:25:26'),('9aac6bbcf52d943bd8dd677eef2874ba1a4c0cdc5eb5e22cc2beab9d33012b02473f9c9507bdb8bb',1,1,'Laravel','[]',0,'2020-11-15 07:38:53','2020-11-15 07:38:53','2021-11-15 07:38:53'),('9c13ffde2b11c620f46007660f84f55cdb46f2a19e3e9476d53768deab430b1a8c669e67083712f5',1,1,'Laravel','[]',0,'2020-11-15 07:55:40','2020-11-15 07:55:40','2021-11-15 07:55:40'),('9d7b1ee7b10a8bf08975b7a9b65de83eccf18edd04848a37ed6d07cf3eb989baa692220456100621',1,1,'Laravel','[]',0,'2020-11-15 07:58:04','2020-11-15 07:58:04','2021-11-15 07:58:04'),('9d9947c8e9f9f2cda448fe1eed0620f60c3347a1fa329a630ff9242f688bc8de6fec42fff09c3062',1,1,'Laravel','[]',0,'2020-11-15 09:30:27','2020-11-15 09:30:27','2021-11-15 09:30:27'),('a32af11a2ca9650f065c220c3eb0d7a697127c0b1ee347500a2f5491e334a5cf3e26b33cf08bf4fc',1,1,'Laravel','[]',0,'2020-11-15 07:41:33','2020-11-15 07:41:33','2021-11-15 07:41:33'),('a7345fdb903a721bff29af9603c13f393aefa97657e0e5b665d0d934ec91e696becddc9bee50e6e3',1,1,'Laravel','[]',0,'2020-11-18 21:37:27','2020-11-18 21:37:27','2021-11-18 21:37:27'),('a76d43e19087219e42dfbfdd2b723a0742eaa5c134b9036b57d85f2b0a0c5c30c8fdbfca30deb8a9',1,1,'Laravel','[]',0,'2020-11-15 09:14:36','2020-11-15 09:14:36','2021-11-15 09:14:36'),('abc4cff41059edf66b6b51e307fb19a104c0d48f0022e83bc8ee28e3d96aa0b4c98cdc646ec0e3a7',1,1,'Laravel','[]',0,'2020-11-15 10:59:19','2020-11-15 10:59:19','2021-11-15 10:59:19'),('b3a93739e34a0de4ec17b95038b396c5fb025055edf6766e59105c059c675b1d46d73dae0f816f2b',1,1,'Laravel','[]',1,'2020-11-16 18:24:00','2020-11-16 18:24:00','2021-11-16 18:24:00'),('b3cb4b56a24ed3c40566e211e7fbf3407fcc3cf61da49c482498ea434b060022400f172aedc5e71d',1,1,'Laravel','[]',0,'2020-11-15 08:08:11','2020-11-15 08:08:11','2021-11-15 08:08:11'),('b564f4f86a2c34f9e8c13cc635e0e8efc54fa56fb02e084d94c87083aa988cc45a0404af88e2c36c',1,1,'Laravel','[]',0,'2020-11-15 10:39:33','2020-11-15 10:39:33','2021-11-15 10:39:33'),('b92ba7c66c043f84f067995f0d8903840662ac76fb9f91a9817ae297cfd308414a8fa9db87a6080a',1,1,'Laravel','[]',0,'2020-11-15 10:51:26','2020-11-15 10:51:26','2021-11-15 10:51:26'),('bd5ef83b1fecbbfc9a7f724685df110ebc6a7419ea8a5dd9615888511e3bfb2c1bfc614f0f64b666',1,1,'Laravel','[]',0,'2020-11-15 10:37:29','2020-11-15 10:37:29','2021-11-15 10:37:29'),('bd78ade13c9838eb461081636c146c7e87f5daa7e597f4a8f9f925ea933d4d26b8766389d776e40e',1,1,'Laravel','[]',0,'2020-11-15 07:43:41','2020-11-15 07:43:41','2021-11-15 07:43:41'),('c199cc81b9fe9db3b6f2dab4a4788d5aabb22624fad7fba1fb4d7d07082920adc200f6b6cfb2eda9',1,1,'Laravel','[]',0,'2020-11-15 09:27:29','2020-11-15 09:27:29','2021-11-15 09:27:29'),('c31050c87cfc1cd4a3cf4e48affd45d5ef249d115c754b488da054ef0ac1a60740b99234f96bb884',1,1,'Laravel','[]',0,'2020-11-15 09:05:44','2020-11-15 09:05:44','2021-11-15 09:05:44'),('c62a80c79d25ef94df4cda8c9ba4d8efb96f470a3a295a4b4ab45695408138f62f16a1e969bb080c',1,1,'Laravel','[]',0,'2020-11-14 11:00:58','2020-11-14 11:00:58','2021-11-14 11:00:58'),('c9dc95405a250a16be7784bf52adb6fea22c4bc44dbb38473fe8e860bf58722afcefa5514778489e',1,1,'Laravel','[]',0,'2020-11-14 10:54:30','2020-11-14 10:54:30','2021-11-14 10:54:30'),('caf138b3499ed86695149e35d4f495fec591116819f265487b339321729b0715389f176c94f240a7',1,1,'Laravel','[]',0,'2020-11-15 11:12:37','2020-11-15 11:12:37','2021-11-15 11:12:37'),('cbcf225f18087678619e829df5220877d25766fa9165ec47932f00476b2fa602f5e3f4c9c3c3238b',1,1,'Laravel','[]',0,'2020-11-15 09:49:56','2020-11-15 09:49:56','2021-11-15 09:49:56'),('cdf476e09177598c8dbe2941460116f0c5b804fa5a964862de3046410c2982e09e25ea932097f358',1,1,'Laravel','[]',0,'2020-11-15 04:24:20','2020-11-15 04:24:20','2021-11-15 04:24:20'),('cf1dfa7a3b0808b39f142f667bec159803ebd7fbd61b6e047d28a45de95f18a37c7976e4494339ca',1,1,'Laravel','[]',0,'2020-11-15 09:43:17','2020-11-15 09:43:17','2021-11-15 09:43:17'),('d257f77b11b15ec17296b30d8504ecb4b8e538e052c91455df9e6ce2242226254cef65916d3d5408',1,1,'Laravel','[]',0,'2020-11-14 10:55:34','2020-11-14 10:55:34','2021-11-14 10:55:34'),('d2e519795e2f2bfb50d3caf54c18fe0b8ae2bd09b743e7cb02399bd95230ea7133ea7d78284ef4d9',1,1,'Laravel','[]',0,'2020-11-15 08:21:33','2020-11-15 08:21:33','2021-11-15 08:21:33'),('d39f507a05252ae63643a5913bbb357197919c02d865735eea0626821516cfa7caec28ca1c288931',1,1,'Laravel','[]',0,'2020-11-15 09:53:05','2020-11-15 09:53:05','2021-11-15 09:53:05'),('d4a91e988fe0f652242e144b7398fdbd249eb02ce25403ea7a56f3323da35da74281269a1d61340c',1,1,'Laravel','[]',0,'2020-11-15 09:39:08','2020-11-15 09:39:08','2021-11-15 09:39:08'),('d5a28542c610cafc78f21796d61ad67455fd0c6ccb2b5a5393260c60ad30512038e2bb2e04d0a4b9',1,1,'Laravel','[]',0,'2020-11-15 18:01:12','2020-11-15 18:01:12','2021-11-15 18:01:12'),('d79138a305b7fc3c4b0831672c40a6e847859ca34f3827c64f6b4a936d69bc3438fa05ee87e6e0e4',1,1,'Laravel','[]',0,'2020-11-15 08:55:02','2020-11-15 08:55:02','2021-11-15 08:55:02'),('db8cedd39c9aaae3122d9ccf14b01b05dc3812dc27a63175ad6e11af490c91241008e6d7731df9ac',1,1,'Laravel','[]',0,'2020-11-15 08:52:53','2020-11-15 08:52:53','2021-11-15 08:52:53'),('deead0795a836528f754c01c1d9d03dac2bbd86583c925f27d6e1561f294ceb130ea6fd11dafd230',1,1,'Laravel','[]',0,'2020-11-15 09:42:40','2020-11-15 09:42:40','2021-11-15 09:42:40'),('e1ddeff0d038fbd55d7a29420f80b069d7267ee06417c82b087e29f8174174b3f58bab4b527326c5',1,1,'Laravel','[]',0,'2020-11-15 04:24:14','2020-11-15 04:24:14','2021-11-15 04:24:14'),('e51167464e2bf5ffca110f62bca235bb7cd9b6a6c199bafb7d37441088663ea1b95800010c6c0b72',1,1,'Laravel','[]',0,'2020-11-15 08:29:30','2020-11-15 08:29:30','2021-11-15 08:29:30'),('e60fcd2879ae1607ba57d411dc2636b3bf5c9578a8373399331dfb0645d08a4f5022c7477cda6f47',1,1,'Laravel','[]',0,'2020-11-15 08:57:15','2020-11-15 08:57:15','2021-11-15 08:57:15'),('e6c254372aefe1ee2ede0b7422e7977a6ef6da47834f8aafa95febb4f5503ee022d7f0ccc0b1dd34',1,1,'Laravel','[]',0,'2020-11-15 07:24:56','2020-11-15 07:24:56','2021-11-15 07:24:56'),('ea57b5e7bf9579e4eac37877f13ce0aad4ff528e7f2e65489971a70271e94c83c8441b7694d52882',1,1,'Laravel','[]',0,'2020-11-15 09:20:55','2020-11-15 09:20:55','2021-11-15 09:20:55'),('eaa02f8f5277097af6f98e359c8e1cf80b53d2e40aee0200223ac27fa22b2f555227bc62a6ed582a',1,1,'Laravel','[]',0,'2020-11-15 11:29:00','2020-11-15 11:29:00','2021-11-15 11:29:00'),('ebb4821548f0ab30e573e1dc127b5c502722e61019884d40c30af3812c9777348870f8264c1d67c0',1,1,'Laravel','[]',1,'2020-11-16 18:12:46','2020-11-16 18:12:46','2021-11-16 18:12:46'),('ebfc43322502de6827f4898fecbc52b3c19833da251669ea6d723aa4a0492f8c6ace7a7f81307946',1,1,'Laravel','[]',1,'2020-11-15 19:12:26','2020-11-15 19:12:26','2021-11-15 19:12:26'),('f1594b7266fd3d02c409b3af7c6fe3aa5f7c678063dc4ff5e242ecbe370ace5a59c60721b25b232e',1,1,'Laravel','[]',0,'2020-11-15 04:24:50','2020-11-15 04:24:50','2021-11-15 04:24:50'),('f1eda8e16865d7d31067e009134022797c071177374db02701ff15909034f099eb52aea159ea6156',1,1,'Laravel','[]',0,'2020-11-15 10:48:56','2020-11-15 10:48:56','2021-11-15 10:48:56'),('f37802278e36c171361a9b3c2ccd42c74a93f4742481da31a5227ca1ba3f77b047639c7c7d831917',1,1,'Laravel','[]',1,'2020-11-15 18:01:36','2020-11-15 18:01:36','2021-11-15 18:01:36'),('f498ca24ba026a1b6f90a9c76bd1394b005ff47dcf6f9e71859204d28043424f14fc10838a74dbcd',1,1,'Laravel','[]',0,'2020-11-14 11:01:29','2020-11-14 11:01:29','2021-11-14 11:01:29'),('f5eb2d53758028bd6d050cdfa3bba016f8432f2b0a45235d64464896e8f12c90f704c3ccd9d6f924',1,1,'Laravel','[]',0,'2020-11-14 10:56:55','2020-11-14 10:56:55','2021-11-14 10:56:55'),('f870edd14b3d2d95fde3fb001f200c22d348e546f98c4521e838040b1a58402578eab6ee7199192d',1,1,'Laravel','[]',0,'2020-11-15 09:13:05','2020-11-15 09:13:05','2021-11-15 09:13:05'),('f9092cd39cc9d9025b9e0b9f233a0c5b46efa196021e5f64060d82e1a4d5fadb17c5047d32ff8530',1,1,'Laravel','[]',0,'2020-11-15 09:56:15','2020-11-15 09:56:15','2021-11-15 09:56:15'),('f986225e2f21cdb465997af68578b39ef1c4e99b961841ff27e91cca88b4e4567f8d3fdef19fcc2b',1,1,'Laravel','[]',0,'2020-11-15 10:31:31','2020-11-15 10:31:31','2021-11-15 10:31:31'),('ffcbe18e8ae1e0f3b13e2dafb42750429c938fced503878aef3671f89ae0dbb88909cdde30c9d332',1,1,'Laravel','[]',0,'2020-11-15 07:18:07','2020-11-15 07:18:07','2021-11-15 07:18:07');

/*Table structure for table `oauth_auth_codes` */

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_auth_codes` */

/*Table structure for table `oauth_clients` */

CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_clients` */

insert  into `oauth_clients`(`id`,`user_id`,`name`,`secret`,`provider`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`) values (1,NULL,'Laravel Personal Access Client','Dn6Okc1RLgtBODNmYRlDq477uhr7DPW0vPTkTMEs',NULL,'http://localhost',1,0,0,'2020-11-12 19:58:43','2020-11-12 19:58:43'),(2,NULL,'Laravel Password Grant Client','I7xHl1sBL6ssAN5zUeu82VoiKw8vUZHueAig93Hf','users','http://localhost',0,1,0,'2020-11-12 19:58:44','2020-11-12 19:58:44');

/*Table structure for table `oauth_personal_access_clients` */

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_personal_access_clients` */

insert  into `oauth_personal_access_clients`(`id`,`client_id`,`created_at`,`updated_at`) values (1,1,'2020-11-12 19:58:43','2020-11-12 19:58:43');

/*Table structure for table `oauth_refresh_tokens` */

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_refresh_tokens` */

/*Table structure for table `package_managments` */

CREATE TABLE `package_managments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charges` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `package_managments` */

insert  into `package_managments`(`id`,`package_name`,`charges`,`created_at`,`updated_at`) values (1,'Super',300,'2020-11-19 19:09:10','2020-11-19 19:09:22'),(2,'Super Cool',400,'2020-11-19 19:09:14','2020-11-19 19:09:24'),(3,'Super Super Cool',500,'2020-11-19 19:09:17','2020-11-19 19:09:27');

/*Table structure for table `password_resets` */

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`id`,`permission_id`,`role_id`,`created_at`,`updated_at`,`deleted_at`) values (1,1,1,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(2,2,1,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(3,3,1,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(4,4,1,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL);

/*Table structure for table `permission_user` */

CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_user` */

/*Table structure for table `permissions` */

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`slug`,`description`,`model`,`created_at`,`updated_at`,`deleted_at`) values (1,'Can View Users','view.users','Can view users','Permission','2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(2,'Can Create Users','create.users','Can create new users','Permission','2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(3,'Can Edit Users','edit.users','Can edit users','Permission','2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(4,'Can Delete Users','delete.users','Can delete users','Permission','2020-11-14 10:13:39','2020-11-14 10:13:39',NULL);

/*Table structure for table `role_user` */

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`id`,`role_id`,`user_id`,`created_at`,`updated_at`,`deleted_at`) values (1,1,1,NULL,NULL,NULL),(2,2,3,'2020-11-18 20:57:18','2020-11-18 20:57:18',NULL),(3,2,4,'2020-11-18 20:57:18','2020-11-18 20:57:18',NULL);

/*Table structure for table `roles` */

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`slug`,`description`,`level`,`created_at`,`updated_at`,`deleted_at`) values (1,'Admin','public','Admin Role',5,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(2,'User','user','User Role',1,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL),(3,'Unverified','unverified','Unverified Role',0,'2020-11-14 10:13:39','2020-11-14 10:13:39',NULL);

/*Table structure for table `subscription_logs` */

CREATE TABLE `subscription_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `expiry_date` date NOT NULL,
  `charges` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subscription_logs` */

insert  into `subscription_logs`(`id`,`employee_id`,`package_id`,`expiry_date`,`charges`,`status`,`created_at`,`updated_at`) values (1,1,1,'2020-11-30','200',1,'2020-11-19 17:33:53','2020-11-19 17:33:57');

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `forgot_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'avatar.jpg',
  `phone_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block_status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`deleted_at`,`forgot_code`,`image`,`phone_code`,`phone`,`address`,`country`,`state`,`city`,`zip_code`,`first_name`,`last_name`,`block_status`) values (1,'public','public@public.com',NULL,'$2y$10$zcNxOM7D2nUuGNidDfxGx.4QhCcyXOTO2WEqwf8heMNmXwc59PmLu',NULL,NULL,NULL,NULL,NULL,'avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',0),(3,'test test','test@gmail.com',NULL,'$2y$10$2HllVzLQXMp6WZeAMRx2IePYQP.Z/fm8yD1EYm5.SnozyMjXNHz0q',NULL,'2020-11-18 20:57:18','2020-11-19 06:52:56',NULL,NULL,'2020-11-18-20-57-18-41703-rick-and-morty-mr-meeseeks-wallpaper-rick-and-morty-39567951-1595-896.jpg',NULL,'test','test','test','test','test','75210','test','test',0),(4,'test test','test1@gmail.com',NULL,'$2y$10$2HllVzLQXMp6WZeAMRx2IePYQP.Z/fm8yD1EYm5.SnozyMjXNHz0q',NULL,'2020-11-18 20:57:18','2020-11-19 06:53:57',NULL,NULL,'2020-11-18-20-57-18-41703-rick-and-morty-mr-meeseeks-wallpaper-rick-and-morty-39567951-1595-896.jpg',NULL,'test','test','test','test','test','75210','test','test',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
