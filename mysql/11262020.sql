/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.11-MariaDB : Database - intelect_bay
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`intelect_bay` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `intelect_bay`;

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'avatar.jpg',
  `phone_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institute_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `block_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `employees` */

insert  into `employees`(`id`,`first_name`,`last_name`,`email`,`image`,`phone_code`,`phone`,`address`,`country`,`state`,`city`,`zip_code`,`institute_id`,`package_id`,`block_status`,`created_at`,`updated_at`) values
(1,'test','test','test@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1,'2020-11-23 11:22:58','2020-11-20 14:27:58'),
(2,'test','test','test1@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1,'2020-11-23 11:23:00','2020-11-20 14:31:53'),
(3,'test','test','test2@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,0,'2020-11-23 11:23:01','2020-11-20 14:35:30'),
(4,'test','test','test3@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,0,'2020-11-23 11:23:04',NULL),
(5,'test','test','test4@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,'2020-11-15 11:23:06',NULL),
(6,'test','test','test5@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,2,0,'2020-11-15 11:23:11',NULL),
(7,'test','test','test6@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,1,0,'2020-11-16 11:23:14',NULL),
(8,'test','test','test7@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,2,0,'2020-11-16 11:23:17',NULL),
(9,'test','test','test8@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,1,0,'2020-11-17 11:23:19',NULL),
(10,'test','test','test9@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,2,0,'2020-11-17 11:23:22',NULL),
(11,'test','test','test10@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,'2020-11-17 11:23:25',NULL),
(12,'test','test','test11@gmail.com','avatar.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2,0,'2020-11-17 11:23:27',NULL);

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `institute_managments` */

DROP TABLE IF EXISTS `institute_managments`;

CREATE TABLE `institute_managments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `institute_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `institute_managments` */

insert  into `institute_managments`(`id`,`institute_name`,`status`,`created_at`,`updated_at`) values
(1,'SalSoft Technology',1,'2020-11-23 11:22:13','2020-11-23 11:22:18'),
(2,'HBL Bank',1,'2020-11-23 11:22:16','2020-11-23 11:22:21');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2016_01_15_105324_create_roles_table',1),
(4,'2016_01_15_114412_create_role_user_table',1),
(5,'2016_01_26_115212_create_permissions_table',1),
(6,'2016_01_26_115523_create_permission_role_table',1),
(7,'2016_02_09_132439_create_permission_user_table',2),
(8,'2016_06_01_000001_create_oauth_auth_codes_table',2),
(9,'2016_06_01_000002_create_oauth_access_tokens_table',2),
(10,'2016_06_01_000003_create_oauth_refresh_tokens_table',2),
(11,'2016_06_01_000004_create_oauth_clients_table',2),
(12,'2016_06_01_000005_create_oauth_personal_access_clients_table',2),
(13,'2019_08_19_000000_create_failed_jobs_table',2),
(14,'2020_11_14_104942_alter_users_add_soft_detele',2),
(15,'2020_11_16_080915_alter_add_forgot_code_users',3),
(16,'2020_11_16_145542_alter_add_address_details_users',4),
(18,'2020_11_17_123720_create_package_managments_table',5),
(19,'2020_11_18_054555_alter_users_table',6),
(20,'2020_11_18_103922_alter_add_block_status_users_table',7),
(21,'2020_11_19_110618_create_employees_table',8),
(22,'2020_11_19_112316_create_subscription_logs_table',8),
(23,'2020_11_20_051357_create_questionaires_table',8),
(24,'2020_11_20_145432_create_user_career_details_table',9),
(25,'2020_11_23_061621_create_institute_managements_table',10);

/*Table structure for table `oauth_access_tokens` */

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_access_tokens` */

insert  into `oauth_access_tokens`(`id`,`user_id`,`client_id`,`name`,`scopes`,`revoked`,`created_at`,`updated_at`,`expires_at`) values
('0036dfb37bed6795a585bfa3c7190093efc9b23ec802ece145ac681e0073e21b861669ce84796616',1,5,'Laravel Personal Access Client','[]',0,'2020-11-16 07:53:19','2020-11-16 07:53:19','2021-11-16 07:53:19'),
('01e4bce8293d421d53006131d8e8e57faa78f31763eb6dbc428507e9b401b201e8a05bba3ab12cbb',1,5,'Laravel','[]',0,'2020-11-26 10:55:34','2020-11-26 10:55:34','2021-11-26 10:55:34'),
('01fe605431c7c7da68d14cfc51493078c1dabc2960d4c0b515f13b091cbdac0a1c55ce0721ebb1c9',1,5,'Laravel','[]',1,'2020-11-26 11:42:30','2020-11-26 11:42:30','2021-11-26 11:42:30'),
('0781b49c1a6538bab70e328d74194ee55d5523ec8ec557dbd9edc16daf32e114f15f1227dfbcdb8f',1,5,'Laravel','[]',0,'2020-11-25 15:58:52','2020-11-25 15:58:52','2021-11-25 15:58:52'),
('09b99959448a77455e6f94bdafd08f7d70c0d5ace94990b1883c4228c16117544a91f50c62dac170',1,5,'Laravel','[]',1,'2020-11-16 10:47:59','2020-11-16 10:47:59','2021-11-16 10:47:59'),
('13abda3371668a97747f1c609d2f833160881c65b9701c0f281774bdd48e3a290f82356449ea20b2',1,5,'Laravel','[]',1,'2020-11-16 12:21:57','2020-11-16 12:21:57','2021-11-16 12:21:57'),
('1413eff882a5bd6ec7181f57a9630d794ae093cba02297bcbbd1f4747bc05ac35b0dd07dfd127bfd',1,5,'Laravel','[]',0,'2020-11-23 11:19:30','2020-11-23 11:19:30','2021-11-23 11:19:30'),
('1460c9645fc885ac0fab43dc1b1af969986d7d77ecbb1c2201664633446ee8a5a29f0066441884f5',1,5,'Laravel','[]',0,'2020-11-26 10:26:42','2020-11-26 10:26:42','2021-11-26 10:26:42'),
('18b74aedf2153814b69be2103c1c0c6ed125dcd8772652e3ff22ee1dd9a1824379f4ac6de2d7ee8d',1,5,'Laravel','[]',1,'2020-11-16 11:12:29','2020-11-16 11:12:29','2021-11-16 11:12:29'),
('1e348fe3b04d9bb089664650102b0075abcf5ed509fccb2f6ae60994b36bc001e31656cd304da1c5',1,5,'Laravel','[]',1,'2020-11-16 11:28:17','2020-11-16 11:28:17','2021-11-16 11:28:17'),
('1fc369355ac4327b057e447987015cccd2a16d28c9932f7031c6b5bfa5c07cdbfeba892671ac29c7',1,5,'Laravel','[]',0,'2020-11-26 11:33:48','2020-11-26 11:33:48','2021-11-26 11:33:48'),
('21a7e48f21f20cd7e76a71d5112f56e97c30855cbecfee5167d67704056be04cba51945b86eb733c',1,5,'Laravel','[]',1,'2020-11-16 11:25:12','2020-11-16 11:25:12','2021-11-16 11:25:12'),
('241f53758c2d0cc78bb621d8ce9cb366fb9886e0e4a81cca96662afb8eddf251b72708514572e084',1,5,'Laravel','[]',0,'2020-11-26 09:18:45','2020-11-26 09:18:45','2021-11-26 09:18:45'),
('248b27ed727c4e86b1b5b78ba0d4123855a6c1450884226d9d817477a81cf7537f1464ee65007a29',1,5,'Laravel','[]',1,'2020-11-16 12:43:38','2020-11-16 12:43:38','2021-11-16 12:43:38'),
('2804c4e47954c45ce2692a9070e8b4cc42efb1bbeeb491fefbc9fa1ee34679224e8afde7e57c3d8b',1,5,'Laravel','[]',1,'2020-11-18 05:33:20','2020-11-18 05:33:20','2021-11-18 05:33:20'),
('2d6a6369cc8c9ebd0357d177a31c71b0a08b286bb6a6400f920553101c29999fa8de783eb46e0b99',1,5,'Laravel','[]',1,'2020-11-16 09:54:27','2020-11-16 09:54:27','2021-11-16 09:54:27'),
('2dad56238621fe3db56f3f03def741889df7ba719d1c541c01073dd0ec703f462aa76a0977612d8f',1,5,'Laravel','[]',1,'2020-11-17 10:45:20','2020-11-17 10:45:20','2021-11-17 10:45:20'),
('329be6337ea3af51d83d6dd6c80e909d30f414a6035177646729df51dec8f86fd2ba4c35f4c330d4',1,5,'Laravel','[]',0,'2020-11-25 15:58:48','2020-11-25 15:58:48','2021-11-25 15:58:48'),
('349a047758f3613ba6cd129729a064e245bd41e7cbd06321038be44575db42fbab24589f5e1d95ed',1,5,'Laravel','[]',1,'2020-11-16 11:05:02','2020-11-16 11:05:02','2021-11-16 11:05:02'),
('365589c24aae5e8e2235e1001afa01e89268202ddefc78caec961837189d69003323d931bdcc2dc6',1,5,'Laravel','[]',1,'2020-11-17 06:28:22','2020-11-17 06:28:22','2021-11-17 06:28:22'),
('3a7d7047954904746e2bd774a04b9aa1084ddd1bb366ee110a68e1ab552d321cda0b583533618ce5',1,5,'Laravel','[]',1,'2020-11-16 09:52:53','2020-11-16 09:52:53','2021-11-16 09:52:53'),
('3b0edc293d417b6966aa7ba541501a5973f853964ae686f06d00aac3908f83050a58ca3ab91dbcb4',1,5,'Laravel','[]',1,'2020-11-16 12:13:19','2020-11-16 12:13:19','2021-11-16 12:13:19'),
('3bc036364182af09bf8e07c2b3b9c1d94d7b97023056978ec8ab6b53e1a0c32ed73942772d4d55cc',1,5,'Laravel','[]',0,'2020-11-16 10:00:52','2020-11-16 10:00:52','2021-11-16 10:00:52'),
('3f7a2cb3670fd7441d25f9a2aadb6892ad7c8eafb0de529ad912982a0a2123c0adc217da3c54bb54',1,5,'Laravel','[]',1,'2020-11-16 10:07:27','2020-11-16 10:07:27','2021-11-16 10:07:27'),
('411173e5ed8186e2ca8599549d91b52980cc4ad0e52ed9bc99302b8b65b730558bedd31ba2d7b7a1',1,5,'Laravel','[]',0,'2020-11-24 06:56:35','2020-11-24 06:56:35','2021-11-24 06:56:35'),
('43987b02448d36397f0eaf35a6ab5a905d3b1b201b5ed4a48cd5525b5f5bac908c03bdb5f7468693',1,5,'Laravel','[]',1,'2020-11-16 13:12:20','2020-11-16 13:12:20','2021-11-16 13:12:20'),
('44e872b8d44a7b4f8ba8962c24c6506e1dd3b01c07c93bd81cccb7772522754e010a2f89f9a2d04d',1,5,'Laravel','[]',1,'2020-11-16 10:24:49','2020-11-16 10:24:49','2021-11-16 10:24:49'),
('471b2ca60a5190476a0455f74a643dc60633db2fba65892d02340f68d8d203dad1ff96248534044a',1,5,'Laravel','[]',1,'2020-11-16 11:11:17','2020-11-16 11:11:17','2021-11-16 11:11:17'),
('491b71dd8d77ee042e5709dd7b8b9bbc61eaf10d098747ca33b57ce506327e4d4032aa6cc363e098',1,5,'Laravel','[]',0,'2020-11-26 13:20:45','2020-11-26 13:20:45','2021-11-26 13:20:45'),
('4bd9494b89b4851bcf7f4ad50bf2bb0294fd8e0e1e4aa8e2104259ec2539ccf1306b55c909dfbef2',1,5,'Laravel','[]',1,'2020-11-16 12:17:16','2020-11-16 12:17:16','2021-11-16 12:17:16'),
('5058bcd45410345323313bfd37e29cb0dcd99f3794a0f1201b703ff180b866b0bb31088f26ae95c9',1,5,'Laravel','[]',1,'2020-11-16 12:28:51','2020-11-16 12:28:51','2021-11-16 12:28:51'),
('50605c84d23e22402f78fac06daacfcca36dad7c19fd7ec489f3221f12b05d15c8b9618bd0a6f881',1,5,'Laravel','[]',1,'2020-11-26 11:07:47','2020-11-26 11:07:47','2021-11-26 11:07:47'),
('532897ce1a380f6b9f4b82e8b441dfcdf2cda23db4223455b1cd753fcf88a84a1bf37abc12ddfa24',1,5,'Laravel','[]',1,'2020-11-17 06:49:21','2020-11-17 06:49:21','2021-11-17 06:49:21'),
('54deeb4146d3ce53cc3b516e0dba2d4ef14fa6b2610913b8c05809eb01f6a9564e5f2910e0fd70f7',1,5,'Laravel','[]',1,'2020-11-16 13:42:44','2020-11-16 13:42:44','2021-11-16 13:42:44'),
('560098e6880a020e61653cdd3f419be17d6111dd0d1745c135af093a969954844079c48b4ddfd08e',1,5,'Laravel','[]',1,'2020-11-16 12:14:58','2020-11-16 12:14:58','2021-11-16 12:14:58'),
('571c4fbbf91934b12ee6f9eb9fe6f7e3d7c359871f3f043d15ea5c433da6156d20169268db6d3371',1,5,'Laravel','[]',1,'2020-11-24 06:47:13','2020-11-24 06:47:13','2021-11-24 06:47:13'),
('587b1490374bc1951ebb855d4a968475a7ffba46ce23f4513c7951bacd16e7660793c5d9015d1dfb',1,5,'Laravel','[]',0,'2020-11-25 15:54:27','2020-11-25 15:54:27','2021-11-25 15:54:27'),
('665c66bc93a097c459935907fff69ab391b0b378202a7ad601767edfc390d8abe6906c2bd6b0fadc',1,5,'Laravel','[]',0,'2020-11-26 10:53:03','2020-11-26 10:53:03','2021-11-26 10:53:03'),
('6aae9ef79a1b52d96ef950e8881c7ef5b169158d8a6445abe8d70c4ba5aeb6a5b7b55a24ee983afc',1,5,'Laravel','[]',0,'2020-11-26 10:17:51','2020-11-26 10:17:51','2021-11-26 10:17:51'),
('6ad58477c0974c2d957d5d190e8393d8535d9950dbff52b61c79aabff5334e076b0338741fe7b800',1,5,'Laravel','[]',0,'2020-11-26 10:17:19','2020-11-26 10:17:19','2021-11-26 10:17:19'),
('72b02de0427d2fe2e202c0b05cf65d263770f25d8f89be6c68f7e204c9fcbf7f3e0f1966f047c8f2',1,5,'Laravel','[]',0,'2020-11-26 13:26:01','2020-11-26 13:26:01','2021-11-26 13:26:01'),
('72b15eab583d0f9605d855476d8315c391adffb48b16169b682dd973fa96f0ceb3bba579b5da7d48',1,5,'Laravel','[]',1,'2020-11-18 05:29:49','2020-11-18 05:29:49','2021-11-18 05:29:49'),
('74cf9d9cf0be6ad9e6e04e16f897832b80f7ed43a3d2afc1fc5717a7941e650d4cf57e86b747a02d',1,5,'Laravel','[]',1,'2020-11-16 12:39:40','2020-11-16 12:39:40','2021-11-16 12:39:40'),
('75d4c3a1afd437f9fa529cb773f9c4f7c2d7dd1f6c1535049782fc8c9357440d29e3deeb75f6c7d3',1,5,'Laravel','[]',1,'2020-11-18 05:18:57','2020-11-18 05:18:57','2021-11-18 05:18:57'),
('7601ed9a181be40f67ab11ea862e896009f30e4e1a006745d00e5cbb47513170cd2fb8cc121ace5f',1,5,'Laravel','[]',1,'2020-11-16 11:26:14','2020-11-16 11:26:14','2021-11-16 11:26:14'),
('7e16ce1d8c919a1cd9ecd9b933176bd68c38b8f6b92214f1b677a10976b196be05b72f81494b8ad2',1,5,'Laravel','[]',0,'2020-11-26 09:16:29','2020-11-26 09:16:29','2021-11-26 09:16:29'),
('7f5d462d25a5ca87936226547734700439426a27a0b5749a4c6209fce0f55531fec8d46cf834e6a3',1,5,'Laravel','[]',0,'2020-11-17 06:21:18','2020-11-17 06:21:18','2021-11-17 06:21:18'),
('851049e76fe1f6e64a60eccf8b921ab5dd45604b6611ac341c2b14c752141f3a05bd25d894acfef6',1,5,'Laravel','[]',0,'2020-11-26 10:48:07','2020-11-26 10:48:07','2021-11-26 10:48:07'),
('87d22058bcc07819dbdb55668593384925ed4f4b214c293e7fdef8fcb084cfe9c7a952b36a5c4829',1,5,'Laravel','[]',0,'2020-11-16 12:20:10','2020-11-16 12:20:10','2021-11-16 12:20:10'),
('8a5e6a32342efefef2728d7c40b4255b510953042b0ff783fef8a7a069f77b6725507518bf8c9075',1,5,'Laravel','[]',1,'2020-11-16 13:43:21','2020-11-16 13:43:21','2021-11-16 13:43:21'),
('8ca4ada174f362da80de49522f46218802052be2cf049a33e51624fdde5f0620a8ab47ccd44008fd',1,5,'Laravel','[]',1,'2020-11-23 06:10:57','2020-11-23 06:10:57','2021-11-23 06:10:57'),
('910257c5e85ede2b02004b264e85e4a6893e263459c8b753efed62ce38ee2d0def0340fc8eaefc97',1,5,'Laravel','[]',1,'2020-11-16 10:01:22','2020-11-16 10:01:22','2021-11-16 10:01:22'),
('91b32b93e8eb86ab5d207372c6236b87185968219d33a0abf5a4291c75bea0766d46185419279ba1',1,5,'Laravel','[]',1,'2020-11-26 11:42:15','2020-11-26 11:42:15','2021-11-26 11:42:15'),
('91f223448f629a5dc4461b8a1467e5c889123f1cac4edfba0674cfe8f784c8349ab882b85dcc22dc',1,5,'Laravel','[]',1,'2020-11-17 10:42:51','2020-11-17 10:42:51','2021-11-17 10:42:51'),
('939e127f1030aa5e12ac39b05c112c1bd22521f6dd966e9a07d5700ac556935e90bdab8888792daa',1,5,'Laravel','[]',1,'2020-11-23 06:30:30','2020-11-23 06:30:30','2021-11-23 06:30:30'),
('998b0e800f9de54db631219eb762f1d82d2a6308e9f95fce14e4c9287fe5756eccc1312722e5dddd',1,5,'Laravel','[]',0,'2020-11-26 10:44:31','2020-11-26 10:44:31','2021-11-26 10:44:31'),
('9a9f04d81d3ae2e8000845f5c718e22ce50b83bd1df562215ddd6c62ed54b5529493ac37f3525b2d',1,5,'Laravel','[]',1,'2020-11-16 13:34:21','2020-11-16 13:34:21','2021-11-16 13:34:21'),
('a168e427555596d09e90b23ccd1417f5d559109ceb82d97bffa8182f4972a2c226cfa7a830f20cce',1,5,'Laravel','[]',1,'2020-11-18 05:25:32','2020-11-18 05:25:32','2021-11-18 05:25:32'),
('a2ff93ceb4d8ce0428b9ab76b72fc9c171b1a814fe30ae53422d42f1a476d66de97e912ac58392ff',1,5,'Laravel','[]',1,'2020-11-18 05:27:54','2020-11-18 05:27:54','2021-11-18 05:27:54'),
('a30adcfee66f24c20fb6d11256e60c292c0d71119c2c856a6f86a2dcd91baa34ffa9e079eec74ab3',1,5,'Laravel','[]',0,'2020-11-26 09:11:10','2020-11-26 09:11:10','2021-11-26 09:11:10'),
('a563c5ec1386929ac7b248e1a76a0b68f6bccd2a9836efa1faa5bc09f12cd95e77129c2a465c3d3f',1,5,'Laravel','[]',1,'2020-11-16 10:39:26','2020-11-16 10:39:26','2021-11-16 10:39:26'),
('a6b87e83c83bc72e05dc1e573af0a546c08c67a92b1bf6caa7abf8cf4451aa6b49c04ae2c8643742',1,5,'Laravel','[]',0,'2020-11-26 09:17:46','2020-11-26 09:17:46','2021-11-26 09:17:46'),
('a7062a9632d982de08f82a173bbb7345065b4c36e4937f0c413dc8e2d68e6c429e4538f61700e868',1,5,'Laravel','[]',0,'2020-11-26 10:53:17','2020-11-26 10:53:17','2021-11-26 10:53:17'),
('ab2d21eb44a1fce8f8830dbb62878f0e35d36d588011029f623aafef023ebd831dd2fe620ee4250f',1,5,'Laravel','[]',0,'2020-11-26 13:20:11','2020-11-26 13:20:11','2021-11-26 13:20:11'),
('adc809a62014298dabb6ed566ec4dbc3228342535680a7ccebd9b9092ebcf5e6e68af2f52a5338ba',1,5,'Laravel Personal Access Client','[]',1,'2020-11-16 07:53:58','2020-11-16 07:53:58','2021-11-16 07:53:58'),
('af4dbf767da46beddbd89a0f9ea619e00939e00c77948a6d894ba890bd7844fad2d6a0cdb6445721',1,5,'Laravel','[]',1,'2020-11-16 12:32:12','2020-11-16 12:32:12','2021-11-16 12:32:12'),
('b76b0f300b0b785e95ee4071983c587e68cddf505617b48217ff8ce33a0d20c9113e243763a1e1cf',1,5,'Laravel','[]',0,'2020-11-26 11:48:40','2020-11-26 11:48:40','2021-11-26 11:48:40'),
('bb4a10eee40108254df4300171e61aba2dfa6c7d52962d5d5ad4115b8ab46f4b8a6cfaa55304699a',1,5,'Laravel','[]',0,'2020-11-26 09:00:15','2020-11-26 09:00:15','2021-11-26 09:00:15'),
('bbea045cc559d4cb87700c00dd3c6bab1ae6297318ba123435ed09a71120c16a0d44999fcac23b2e',1,5,'Laravel','[]',0,'2020-11-26 09:01:17','2020-11-26 09:01:17','2021-11-26 09:01:17'),
('cc63e479b6fd520be8f9fdf1bb133fc3b4bfccddf000694d11836541365aaa0a6b609563cc69a48c',1,5,'Laravel','[]',1,'2020-11-16 12:16:33','2020-11-16 12:16:33','2021-11-16 12:16:33'),
('ce5428674c34d724e9cbefc416600daf6fc0a30c9ff0972fe0948c00a7ca5151e43f548bd71a4da8',1,5,'Laravel','[]',1,'2020-11-16 13:26:21','2020-11-16 13:26:21','2021-11-16 13:26:21'),
('ce88f9bec5e3d8c06fd0d15e4eca109150a7fc2e8a4ba9287c5639d431e94e4da203a8c70f47fbed',1,5,'Laravel','[]',1,'2020-11-26 11:01:49','2020-11-26 11:01:49','2021-11-26 11:01:49'),
('ce93acdbf6757d0f1cfab9c84f777505555e614255aded8dea75eb59cb0567529a6c3a41156f13fc',1,5,'Laravel','[]',0,'2020-11-26 10:19:53','2020-11-26 10:19:53','2021-11-26 10:19:53'),
('d0154ad6fd468b844d6d540483c3fc6898c7e1e196fbcac99d6e28eb1e5d27d3055a17ab4d0fec07',1,5,'Laravel','[]',0,'2020-11-26 09:07:16','2020-11-26 09:07:16','2021-11-26 09:07:16'),
('d92fe9188e0f747b73fafbf9b1342e3606fd0a80c55acd83365f6bd7e4febcd34ed3b0d1fb383461',1,5,'Laravel','[]',1,'2020-11-16 10:20:27','2020-11-16 10:20:27','2021-11-16 10:20:27'),
('e044672a6eb41b81908fc21c8bf9a0b440d3e6fde858a0482bf4c20008f439c393e557a8f33ee584',1,5,'Laravel','[]',1,'2020-11-25 15:49:44','2020-11-25 15:49:44','2021-11-25 15:49:44'),
('e26731ccf8c699f8e38ca42d4725dae0ec0549ce33a58b96d20c27d7031ac158a313e87ad7f5f0cb',1,5,'Laravel','[]',1,'2020-11-16 12:12:17','2020-11-16 12:12:17','2021-11-16 12:12:17'),
('e2abd6474aea79977d68ead2a3883f6a02671d41a39a0a8578bb4ee4f6976d52579ca9b2a6449197',1,5,'Laravel','[]',1,'2020-11-18 08:53:33','2020-11-18 08:53:33','2021-11-18 08:53:33'),
('e61b30e0222338c0ab6c4bf7840ed8f2b09eecd09d9a4f8d26d8711c8bfac62bf24b89ac05b1ec1c',1,5,'Laravel','[]',1,'2020-11-16 11:21:31','2020-11-16 11:21:31','2021-11-16 11:21:31'),
('e6e22a881c83311ff65e365c964a48b46e7a553ceaf8069094a2afac84c32c170ace32d85f18788d',1,5,'Laravel','[]',1,'2020-11-16 12:49:35','2020-11-16 12:49:35','2021-11-16 12:49:35'),
('e915b491d3653c1207629df1868cc08f297dac25cead3b1d4926ba46ec242dc348c8fdd58d8f43c5',1,5,'Laravel','[]',0,'2020-11-26 09:19:57','2020-11-26 09:19:57','2021-11-26 09:19:57'),
('eb1b52c7bfca0b778e224dc4d3c37f0c49fe08408f2f18230237b88b6c4ab9a66b71f1cb57d6c601',1,5,'Laravel','[]',0,'2020-11-26 10:16:57','2020-11-26 10:16:57','2021-11-26 10:16:57'),
('f06c4999a4c4047ef6bff5b7b97922ad78abe315cd009c1fe8f6c6b5e1e57e100eb471f90e82916e',1,5,'Laravel','[]',0,'2020-11-20 13:59:53','2020-11-20 13:59:53','2021-11-20 13:59:53'),
('f384afe4767c6a2547845c3266ae3938def11e1b81efed171e2db37d38abb078ae3059b5d98a784f',1,5,'Laravel','[]',1,'2020-11-16 12:34:06','2020-11-16 12:34:06','2021-11-16 12:34:06'),
('ffa43fb3a3d3df4a943dc1a2199b57cc0b7494fb5474aee9a2ca3a75c27f2c94ee3397a587efdaea',1,5,'Laravel','[]',1,'2020-11-16 07:58:18','2020-11-16 07:58:18','2021-11-16 07:58:18');

/*Table structure for table `oauth_auth_codes` */

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_auth_codes` */

/*Table structure for table `oauth_clients` */

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_clients` */

insert  into `oauth_clients`(`id`,`user_id`,`name`,`secret`,`provider`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`) values
(1,NULL,'Laravel Personal Access Client','XwVucuBTyId0fZLJXgeokgNQaBtF7oajNypAzLfy',NULL,'http://localhost',1,0,0,'2020-11-16 06:57:34','2020-11-16 06:57:34'),
(2,NULL,'Laravel Password Grant Client','a6yNb2sLnDXGsqSmFxvukEq2jvJWDURfEG2sxzCZ','users','http://localhost',0,1,0,'2020-11-16 06:57:34','2020-11-16 06:57:34'),
(3,NULL,'Laravel Personal Access Client','WlitFjRaPv0Zcu68l7qKl3fHgwLg5rMjZctCrCiv',NULL,'http://localhost',1,0,0,'2020-11-16 07:26:37','2020-11-16 07:26:37'),
(4,NULL,'Laravel Password Grant Client','WEMooQcHPoPCnOd0f4pG1SvoRl5MuB8tbydjsPQZ','users','http://localhost',0,1,0,'2020-11-16 07:26:37','2020-11-16 07:26:37'),
(5,NULL,'y','kgwKT1uGodPbVcG6P8ukaWqnogSQnooauv3tMdhg',NULL,'http://localhost',1,0,0,'2020-11-16 07:31:30','2020-11-16 07:31:30');

/*Table structure for table `oauth_personal_access_clients` */

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_personal_access_clients` */

insert  into `oauth_personal_access_clients`(`id`,`client_id`,`created_at`,`updated_at`) values
(1,1,'2020-11-16 06:57:34','2020-11-16 06:57:34'),
(2,3,'2020-11-16 07:26:37','2020-11-16 07:26:37'),
(3,5,'2020-11-16 07:31:30','2020-11-16 07:31:30');

/*Table structure for table `oauth_refresh_tokens` */

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_refresh_tokens` */

/*Table structure for table `package_managments` */

DROP TABLE IF EXISTS `package_managments`;

CREATE TABLE `package_managments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charges` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `package_managments` */

insert  into `package_managments`(`id`,`package_name`,`charges`,`created_at`,`updated_at`) values
(1,'Plus Package',200,NULL,'2020-11-18 05:41:04'),
(2,'Super Package',300,NULL,'2020-11-18 05:41:04'),
(3,'Super Duper Package',400,NULL,'2020-11-18 05:41:04');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`id`,`permission_id`,`role_id`,`created_at`,`updated_at`,`deleted_at`) values
(1,1,1,'2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(2,2,1,'2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(3,3,1,'2020-11-16 06:54:51','2020-11-16 06:54:51',NULL),
(4,4,1,'2020-11-16 06:54:51','2020-11-16 06:54:51',NULL);

/*Table structure for table `permission_user` */

DROP TABLE IF EXISTS `permission_user`;

CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_user` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`slug`,`description`,`model`,`created_at`,`updated_at`,`deleted_at`) values
(1,'Can View Users','view.users','Can view users','Permission','2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(2,'Can Create Users','create.users','Can create new users','Permission','2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(3,'Can Edit Users','edit.users','Can edit users','Permission','2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(4,'Can Delete Users','delete.users','Can delete users','Permission','2020-11-16 06:54:50','2020-11-16 06:54:50',NULL);

/*Table structure for table `questionaires` */

DROP TABLE IF EXISTS `questionaires`;

CREATE TABLE `questionaires` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `questions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `questionaires` */

insert  into `questionaires`(`id`,`questions`,`answer_type`,`answers`,`status`,`created_at`,`updated_at`) values
(8,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','checkbox','[\"test\",\"test-21\"]',1,'2020-11-23 15:15:10','2020-11-23 15:15:10'),
(9,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','radio','[\"teet\",\"test-21\"]',1,'2020-11-23 15:15:10','2020-11-23 15:15:10');

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`id`,`role_id`,`user_id`,`created_at`,`updated_at`,`deleted_at`) values
(1,1,1,NULL,NULL,NULL),
(3,2,8,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL),
(5,2,12,NULL,NULL,NULL),
(6,2,13,NULL,NULL,NULL),
(7,2,14,NULL,NULL,NULL),
(8,2,15,NULL,NULL,NULL),
(9,2,16,NULL,NULL,NULL),
(10,2,17,NULL,NULL,NULL),
(11,2,18,NULL,NULL,NULL),
(12,2,19,NULL,NULL,NULL),
(13,2,20,NULL,NULL,NULL),
(14,2,21,NULL,NULL,NULL),
(15,2,22,NULL,NULL,NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`slug`,`description`,`level`,`created_at`,`updated_at`,`deleted_at`) values
(1,'Admin','public','Admin Role',5,'2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(2,'User','user','User Role',1,'2020-11-16 06:54:50','2020-11-16 06:54:50',NULL),
(3,'Unverified','unverified','Unverified Role',0,'2020-11-16 06:54:50','2020-11-16 06:54:50',NULL);

/*Table structure for table `subscription_logs` */

DROP TABLE IF EXISTS `subscription_logs`;

CREATE TABLE `subscription_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `expiry_date` date NOT NULL,
  `charges` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subscription_logs` */

insert  into `subscription_logs`(`id`,`employee_id`,`package_id`,`expiry_date`,`charges`,`status`,`created_at`,`updated_at`) values
(1,1,2,'2020-11-30','300',1,'2020-11-23 11:13:52','2020-11-23 11:13:57'),
(2,2,1,'2020-11-30','200',1,'2020-11-23 11:13:55','2020-11-23 11:14:00'),
(3,3,2,'2020-11-30','300',1,'2020-11-23 11:14:17','2020-11-23 11:14:21');

/*Table structure for table `user_career_details` */

DROP TABLE IF EXISTS `user_career_details`;

CREATE TABLE `user_career_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_career_details` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `forgot_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.jpg',
  `phone_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block_status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`deleted_at`,`forgot_code`,`image`,`phone_code`,`phone`,`address`,`country`,`state`,`city`,`zip_code`,`first_name`,`last_name`,`block_status`) values
(1,'Touqeer Hussain','hosting2k17@gmail.com','2020-11-16 12:03:14','$2y$12$zKjcZNU0W/pPDh4Q59dkfOkKbXGXkDjm4Qt8UgNe.Xy8XMe3/p0OS',NULL,NULL,'2020-11-23 06:11:56',NULL,'3345','2020-11-17-08-09-45-AP3.jpg',NULL,'+923402111701','test','test','test','test','test','Touqeer','Hussain',0),
(8,'test test','test@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-25 15:50:31',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),
(12,'test test','test1@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-25 15:50:24',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(13,'test test','test2@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(14,'test test','test3@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(15,'test test','test4@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(16,'test test','test5@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(17,'test test','test6@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(18,'test test','test7@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(19,'test test','test8@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(20,'test test','test9@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(21,'test test','test10@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(22,'test test','test11@mailintor.com',NULL,'$2y$10$AZctPA1mkp1trFso0yB.ue7qJjbLH3V8WxPB2bGQmxfcpSUlJXZkq',NULL,'2020-11-18 07:33:18','2020-11-18 07:33:18',NULL,NULL,'2020-11-18-07-33-18-3030.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
